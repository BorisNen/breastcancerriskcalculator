import tkinter as tk
import gail_calculator as gail
import numpy as np
from tkinter import ttk
import claus_calculator as claus

BIOPSIES = {
    'Unknown': 1,
    'No biopsies': 0,
    '1 biopsies': 1,
    '2 or more biopsies': 2
}

HAD_BIOPSIES = {
    'Unknown': 99,
    'No biopsies': 0,
    '1 biopsies': 1,
    '2 or more biopsies': 1
}

ATYPICAL_BIOPSIES = {
    'Unknown': 99,
    'No': 0,
    'Yes': 1
}

MENARCH = {
    'Unknown': 0,
    'Less than 12 years old': 2,
    '12 through 13 years old': 1,
    'Greater than 13 years old': 0
}

BIRTH = {
    'Unknown': 0,
    'No births': 2,
    'Less than 20 years old': 0,
    '20 through 24 years old': 1,
    '25 through 29 years old': 2,
    '30 years old and greater': 3
}

RELATIVES = {
    'Unknown': 0,
    '0 relatives': 0,
    '1 relative': 1,
    '2 or more relatives': 2
}

RACE = {
    'Unknown': 1,
    'White': 1,
    'African American': 2,
    'Hispanic': 3,
    'Asian American (Chinese)': 7,
    'Asian American (Japanese)': 8,
    'Asian American (Filipino)': 9,
    'Asian American (Hawaiian)': 10,
    'Asian American (Other Pacific Islander)': 11,
    'Asian American (Other Asian American)': 12,
    'American Indian or Alaskan Native': 1
}


class GUI:
    def __init__(self):
        self.main = tk.Tk()
        self.main.geometry('1250x700')

        self.main.title('Breast Cancer Risk Calculator')

        # Creating widgets
        self.create_widgets()
        self.create_claus_widgets()
        self.hide_claus_widgets()
        self.create_gail_widgets()
        self.gail_button.invoke()

    def create_widgets(self):
        self.questions = ttk.LabelFrame(self.main, text='Patient Questions')
        self.questions.grid(column=0, row=0, padx=2, pady=8, sticky=tk.NSEW, rowspan=2)

        self.radVar = tk.IntVar()
        self.gail_button = ttk.Radiobutton(self.questions, text='GAIL', variable=self.radVar,
                                     value=1, command=self.rad_call)
        self.gail_button.grid(column=0, row=0, padx=2, pady=8, sticky=tk.NSEW)

        claus_model_button = ttk.Radiobutton(self.questions, text='Claus Model', variable=self.radVar,
                                      value=2, command=self.rad_call)
        claus_model_button.grid(column=1, row=0, padx=2, pady=8, sticky=tk.NSEW)

        start_analysis = ttk.Button(self.questions, text='Calculate Risk', command=self.run_gail)
        start_analysis.grid(column=0, row=16, padx=2, pady=20, sticky=tk.NW)

        results = ttk.LabelFrame(self.main, text='Results for the GAIL model')
        results.grid(column=1, row=0, padx=2, pady=8, sticky=tk.NSEW)

        fiveYearAbs = ttk.Label(results, text='1. This is the 5 year risk value for the specified patient.')
        fiveYearAbs.grid(column=0, row=0, padx=2, pady=8, sticky=tk.NW)

        self.fYAresult = ttk.Label(results)
        self.fYAresult.grid(column=1, row=0, padx=2, pady=8, sticky=tk.NW)
        self.fYAresult.grid_remove()

        fiveYearAve = ttk.Label(results, text='2. This is the 5 year risk value for an average patient of '
                                                   'the same age/race.')
        fiveYearAve.grid(column=0, row=1, padx=2, pady=8, sticky=tk.NW)

        self.fYVresult = ttk.Label(results)
        self.fYVresult.grid(column=1, row=1, padx=2, pady=8, sticky=tk.NW)
        self.fYVresult.grid_remove()

        lifeTimeAbs = ttk.Label(results, text='3. This is the lifetime risk value for the specified patient. ('
                                                   'This is calculated with a projection age of 90)')
        lifeTimeAbs.grid(column=0, row=2, padx=2, pady=8, sticky=tk.NW)

        self.lTAresult = ttk.Label(results)
        self.lTAresult.grid(column=1, row=2, padx=2, pady=8, sticky=tk.NW)
        self.lTAresult.grid_remove()

        lifeTimeAve = ttk.Label(results, text='4. This is the lifetime risk value for an average patient of the '
                                                'same age/race. (This is calculated with a projection age of 90)')
        lifeTimeAve.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NW)

        self.lTVresult = ttk.Label(results)
        self.lTVresult.grid(column=1, row=3, padx=2, pady=8, sticky=tk.NW)
        self.lTVresult.grid_remove()

        results_claus = ttk.LabelFrame(self.main, text='Results for the Claus model')
        results_claus.grid(column=1, row=1, padx=2, pady=8, sticky=tk.NSEW)

        lifeTimeClaus = ttk.Label(results_claus, text='1. This is the lifetime risk from the Claus model')
        lifeTimeClaus.grid(column=0, row=0, padx=2, pady=8, sticky=tk.NW)

        self.resultClaus = ttk.Label(results_claus)
        self.resultClaus.grid(column=1, row=0, padx=2, pady=8, sticky=tk.NW)
        self.resultClaus.grid_remove()

    def create_gail_widgets(self):
        self.age_label = ttk.Label(self.questions, text="Choose patient's age (from 35 to 99)")
        self.age_label.grid(column=0, row=2, padx=2, pady=8, sticky=tk.NSEW)

        self.age_spin = tk.Spinbox(self.questions, from_=35, to=99, width=5, bd=6)
        self.age_spin.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NW)

        self.menstruation_label = ttk.Label(self.questions, text="Woman's age at the time of her first menstrual period?")
        self.menstruation_label.grid(column=0, row=4, padx=2, pady=8, sticky=tk.NSEW)

        self.menstruation = tk.StringVar()
        self.menstruation_spin = ttk.Combobox(self.questions, width=24, textvariable=self.menstruation, state='readonly')
        self.menstruation_spin['values'] = (
        'Unknown', 'Less than 12 years old', '12 through 13 years old', 'Greater than 13 years old')
        self.menstruation_spin.grid(column=0, row=5, padx=2, pady=8, sticky=tk.NW)
        self.menstruation_spin.current(0)

        self.birth_label = ttk.Label(self.questions, text="Woman's age at the time of her first birth of a child?")
        self.birth_label.grid(column=0, row=6, padx=2, pady=8, sticky=tk.NSEW)

        self.birth = tk.StringVar()
        self.birth_spin = ttk.Combobox(self.questions, width=24, textvariable=self.birth, state='readonly')
        self.birth_spin['values'] = ('Unknown', 'No births', 'Less than 20 years old', '20 through 24 years old',
                                '25 through 29 years old', '30 years old and greater')
        self.birth_spin.grid(column=0, row=7, padx=2, pady=8, sticky=tk.NW)
        self.birth_spin.current(0)

        self.relatives_label = ttk.Label(self.questions, text="Number of 1st degree relatives that have had breast cancer?")
        self.relatives_label.grid(column=0, row=8, padx=2, pady=8, sticky=tk.NSEW)

        self.relatives = tk.StringVar()
        self.relatives_spin = ttk.Combobox(self.questions, width=24, textvariable=self.relatives, state='readonly')
        self.relatives_spin['values'] = ('Unknown', '0 relatives', '1 relative', '2 or more relatives')
        self.relatives_spin.grid(column=0, row=9, padx=2, pady=8, sticky=tk.NW)
        self.relatives_spin.current(0)

        self.biopsies_label = ttk.Label(self.questions, text="Number of breast biopsies?")
        self.biopsies_label.grid(column=0, row=10, padx=2, pady=8, sticky=tk.NSEW)

        self.biopsies = tk.StringVar()
        self.biopsies_spin = ttk.Combobox(self.questions, width=24, textvariable=self.biopsies, state='readonly')
        self.biopsies_spin['values'] = ('Unknown', 'No biopsies', '1 biopsies', '2 or more biopsies')
        self.biopsies_spin.grid(column=0, row=11, padx=2, pady=8, sticky=tk.NW)
        self.biopsies_spin.current(0)

        self.atypical_biopsies_label = ttk.Label(self.questions,
                                            text="Has the woman had at least one breast biopsy with atypical hyperplasia?")
        self.atypical_biopsies_label.grid(column=0, row=12, padx=2, pady=8, sticky=tk.NSEW)

        self.atypical_biopsies = tk.StringVar()
        self.atypical_biopsies_spin = ttk.Combobox(self.questions, width=24, textvariable=self.atypical_biopsies,
                                              state='readonly')
        self.atypical_biopsies_spin['values'] = ('Unknown', 'No', 'Yes')
        self.atypical_biopsies_spin.grid(column=0, row=13, padx=2, pady=8, sticky=tk.NW)
        self.atypical_biopsies_spin.current(0)

        self.race_label = ttk.Label(self.questions, text="Patient's race?")
        self.race_label.grid(column=0, row=14, padx=2, pady=8, sticky=tk.NSEW)

        self.race = tk.StringVar()
        self.race_spin = ttk.Combobox(self.questions, width=36, textvariable=self.race, state='readonly')
        self.race_spin['values'] = ('Unknown', 'White', 'African American', 'Hispanic', 'Asian American (Chinese)',
                               'Asian American (Japanese)', 'Asian American (Filipino)', 'Asian American (Hawaiian)',
                               'Asian American (Other Pacific Islander)', 'Asian American (Other Asian American)',
                               'American Indian or Alaskan Native')
        self.race_spin.grid(column=0, row=15, padx=2, pady=8, sticky=tk.NW)
        self.race_spin.current(0)

    def hide_gail_widgets(self):
        self.age_label.grid_remove()
        self.age_spin.grid_remove()
        self.menstruation_label.grid_remove()
        self.menstruation_spin.grid_remove()
        self.birth_label.grid_remove()
        self.birth_spin.grid_remove()
        self.relatives_label.grid_remove()
        self.relatives_spin.grid_remove()
        self.biopsies_label.grid_remove()
        self.biopsies_spin.grid_remove()
        self.atypical_biopsies_label.grid_remove()
        self.atypical_biopsies_spin.grid_remove()
        self.race_label.grid_remove()
        self.race_spin.grid_remove()

    def show_gail_widgets(self):
        self.age_label.grid()
        self.age_spin.grid()
        self.menstruation_label.grid()
        self.menstruation_spin.grid()
        self.birth_label.grid()
        self.birth_spin.grid()
        self.relatives_label.grid()
        self.relatives_spin.grid()
        self.biopsies_label.grid()
        self.biopsies_spin.grid()
        self.atypical_biopsies_label.grid()
        self.atypical_biopsies_spin.grid()
        self.race_label.grid()
        self.race_spin.grid()

    def create_claus_widgets(self):
        self.family_member_onset = ttk.Label(self.questions, text='This model calculates the lifetime claus risk score based'
                                                             ' on the age onset of relatives with breast cancer')
        self.family_member_onset.grid(column=0, row=1, padx=2, pady=8, sticky=tk.NSEW, columnspan=5)

        self.patient_age_label = ttk.Label(self.questions, text="Current age of the patient")
        self.patient_age_label.grid(column=0, row=2, padx=2, pady=4, sticky=tk.NSEW)

        self.patient_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.patient_age_spin.grid(column=1, row=2, padx=2, pady=4, sticky=tk.NW)

        self.motherVar = tk.BooleanVar()
        self.mother_label = ttk.Checkbutton(self.questions, text='Mother', var=self.motherVar, command=self.refresh_claus_widgets)
        self.mother_label.grid(column=0, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.daughterVar = tk.BooleanVar()
        self.daughter_label = ttk.Checkbutton(self.questions, text='Daughter', var=self.daughterVar, command=self.refresh_claus_widgets)
        self.daughter_label.grid(column=1, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.sisterVar = tk.BooleanVar()
        self.sister_label = ttk.Checkbutton(self.questions, text='Sister', var=self.sisterVar, command=self.refresh_claus_widgets)
        self.sister_label.grid(column=2, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.mauntVar = tk.BooleanVar()
        self.maunt_label = ttk.Checkbutton(self.questions, text='Maternal aunt', var=self.mauntVar, command=self.refresh_claus_widgets)
        self.maunt_label.grid(column=3, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.pauntVar = tk.BooleanVar()
        self.paunt_label = ttk.Checkbutton(self.questions, text='Paternal aunt', var=self.pauntVar, command=self.refresh_claus_widgets)
        self.paunt_label.grid(column=4, row=3, padx=2, pady=8, sticky=tk.NSEW)

        self.pgrandmotherVar = tk.BooleanVar()
        self.pgrandmother_label = ttk.Checkbutton(self.questions, text='Maternal grandmother', var=self.pgrandmotherVar, command=self.refresh_claus_widgets)
        self.pgrandmother_label.grid(column=0, row=4, padx=2, pady=8, sticky=tk.NSEW)

        self.mgrandmotherVar = tk.BooleanVar()
        self.mgrandmother_label = ttk.Checkbutton(self.questions, text='Paternal grandmother', var=self.mgrandmotherVar, command=self.refresh_claus_widgets)
        self.mgrandmother_label.grid(column=1, row=4, padx=2, pady=8, sticky=tk.NSEW)

        self.mhalfsisterVar = tk.BooleanVar()
        self.mhalfsister_label = ttk.Checkbutton(self.questions, text='Maternal half-sister', var=self.mhalfsisterVar, command=self.refresh_claus_widgets)
        self.mhalfsister_label.grid(column=2, row=4, padx=2, pady=8, sticky=tk.NSEW)

        self.phalfsisterVar  = tk.BooleanVar()
        self.phalfsister_label = ttk.Checkbutton(self.questions, text='Paternal half-sister', var=self.phalfsisterVar, command=self.refresh_claus_widgets)
        self.phalfsister_label.grid(column=3, row=4, padx=2, pady=8, sticky=tk.NSEW)

        self.mother_age_label = ttk.Label(self.questions, text="Mother's onset age")
        self.mother_age_label.grid(column=0, row=5, padx=2, pady=4, sticky=tk.NSEW)

        self.mother_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.mother_age_spin.grid(column=1, row=5, padx=2, pady=4, sticky=tk.NW)

        self.daughter_age_label = ttk.Label(self.questions, text="Daughter's onset age")
        self.daughter_age_label.grid(column=2, row=5, padx=2, pady=4, sticky=tk.NSEW)

        self.daughter_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.daughter_age_spin.grid(column=3, row=5, padx=2, pady=4, sticky=tk.NW)

        self.sister_age_label = ttk.Label(self.questions, text="Sister's onset age")
        self.sister_age_label.grid(column=0, row=6, padx=2, pady=4, sticky=tk.NSEW)

        self.sister_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.sister_age_spin.grid(column=1, row=6, padx=2, pady=4, sticky=tk.NW)

        self.maunt_age_label = ttk.Label(self.questions, text="Maternal aunt's onset age")
        self.maunt_age_label.grid(column=2, row=6, padx=2, pady=4, sticky=tk.NSEW)

        self.maunt_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.maunt_age_spin.grid(column=3, row=6, padx=2, pady=4, sticky=tk.NW)

        self.paunt_age_label = ttk.Label(self.questions, text="Paternal aunt's onset age")
        self.paunt_age_label.grid(column=0, row=7, padx=2, pady=4, sticky=tk.NSEW)

        self.paunt_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.paunt_age_spin.grid(column=1, row=7, padx=2, pady=4, sticky=tk.NW)

        self.mgrandmother_age_label = ttk.Label(self.questions, text="Maternal grandmother's onset age")
        self.mgrandmother_age_label.grid(column=2, row=7, padx=2, pady=4, sticky=tk.NSEW)

        self.mgrandmother_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.mgrandmother_age_spin.grid(column=3, row=7, padx=2, pady=4, sticky=tk.NW)

        self.pgrandmother_age_label = ttk.Label(self.questions, text="Paternal grandmother's onset age")
        self.pgrandmother_age_label.grid(column=0, row=8, padx=2, pady=4, sticky=tk.NSEW)

        self.pgrandmother_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.pgrandmother_age_spin.grid(column=1, row=8, padx=2, pady=4, sticky=tk.NW)

        self.mstepsister_age_label = ttk.Label(self.questions, text="Maternal step sister's onset age")
        self.mstepsister_age_label.grid(column=2, row=8, padx=2, pady=4, sticky=tk.NSEW)

        self.mstepsister_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.mstepsister_age_spin.grid(column=3, row=8, padx=2, pady=4, sticky=tk.NW)

        self.pstepsister_age_label = ttk.Label(self.questions, text="Paternal step sister's onset age")
        self.pstepsister_age_label.grid(column=0, row=9, padx=2, pady=4, sticky=tk.NSEW)

        self.pstepsister_age_spin = tk.Spinbox(self.questions, from_=20, to=79, width=5, bd=6)
        self.pstepsister_age_spin.grid(column=1, row=9, padx=2, pady=4, sticky=tk.NW)

    def hide_claus_widgets(self):
        self.family_member_onset.grid_remove()
        self.patient_age_label.grid_remove()
        self.patient_age_spin.grid_remove()
        self.mother_label.grid_remove()
        self.daughter_label.grid_remove()
        self.sister_label.grid_remove()
        self.maunt_label.grid_remove()
        self.paunt_label.grid_remove()
        self.mgrandmother_label.grid_remove()
        self.pgrandmother_label.grid_remove()
        self.mhalfsister_label.grid_remove()
        self.phalfsister_label.grid_remove()
        self.mother_age_label.grid_remove()
        self.mother_age_spin.grid_remove()
        self.daughter_age_label.grid_remove()
        self.daughter_age_spin.grid_remove()
        self.sister_age_label.grid_remove()
        self.sister_age_spin.grid_remove()
        self.maunt_age_label.grid_remove()
        self.maunt_age_spin.grid_remove()
        self.paunt_age_label.grid_remove()
        self.paunt_age_spin.grid_remove()
        self.mgrandmother_age_label.grid_remove()
        self.mgrandmother_age_spin.grid_remove()
        self.pgrandmother_age_label.grid_remove()
        self.pgrandmother_age_spin.grid_remove()
        self.mstepsister_age_label.grid_remove()
        self.mstepsister_age_spin.grid_remove()
        self.pstepsister_age_label.grid_remove()
        self.pstepsister_age_spin.grid_remove()
        self.motherVar.set(False)
        self.daughterVar.set(False)
        self.sisterVar.set(False)
        self.mauntVar.set(False)
        self.pauntVar.set(False)
        self.mgrandmotherVar.set(False)
        self.pgrandmotherVar.set(False)
        self.mhalfsisterVar.set(False)
        self.phalfsisterVar.set(False)

    def show_claus_widgets(self):
        self.family_member_onset.grid()
        self.patient_age_label.grid()
        self.patient_age_spin.grid()
        self.mother_label.grid()
        self.daughter_label.grid()
        self.sister_label.grid()
        self.maunt_label.grid()
        self.paunt_label.grid()
        self.mgrandmother_label.grid()
        self.pgrandmother_label.grid()
        self.mhalfsister_label.grid()
        self.phalfsister_label.grid()

    def rad_call(self):
        if self.radVar.get() == 1:
            self.hide_claus_widgets()
            self.show_gail_widgets()
        elif self.radVar.get() == 2:
            self.hide_gail_widgets()
            self.show_claus_widgets()

    def refresh_claus_widgets(self):
        if self.motherVar.get():
            self.mother_age_label.grid()
            self.mother_age_spin.grid()
        else:
            self.mother_age_label.grid_remove()
            self.mother_age_spin.grid_remove()

        if self.daughterVar.get():
            self.daughter_age_label.grid()
            self.daughter_age_spin.grid()
        else:
            self.daughter_age_label.grid_remove()
            self.daughter_age_spin.grid_remove()

        if self.sisterVar.get():
            self.sister_age_label.grid()
            self.sister_age_spin.grid()
        else:
            self.sister_age_label.grid_remove()
            self.sister_age_spin.grid_remove()

        if self.mauntVar.get():
            self.maunt_age_label.grid()
            self.maunt_age_spin.grid()
        else:
            self.maunt_age_label.grid_remove()
            self.maunt_age_spin.grid_remove()

        if self.pauntVar.get():
            self.paunt_age_label.grid()
            self.paunt_age_spin.grid()
        else:
            self.paunt_age_label.grid_remove()
            self.paunt_age_spin.grid_remove()

        if self.mgrandmotherVar.get():
            self.mgrandmother_age_label.grid()
            self.mgrandmother_age_spin.grid()
        else:
            self.mgrandmother_age_label.grid_remove()
            self.mgrandmother_age_spin.grid_remove()

        if self.pgrandmotherVar.get():
            self.pgrandmother_age_label.grid()
            self.pgrandmother_age_spin.grid()
        else:
            self.pgrandmother_age_label.grid_remove()
            self.pgrandmother_age_spin.grid_remove()

        if self.mhalfsisterVar.get():
            self.mstepsister_age_label.grid()
            self.mstepsister_age_spin.grid()
        else:
            self.mstepsister_age_label.grid_remove()
            self.mstepsister_age_spin.grid_remove()

        if self.phalfsisterVar.get():
            self.pstepsister_age_label.grid()
            self.pstepsister_age_spin.grid()
        else:
            self.pstepsister_age_label.grid_remove()
            self.pstepsister_age_spin.grid_remove()

    def run_gail(self):
        if self.radVar.get() == 2:
            mother, daughter, sister, maunt, paunt, mgrandmother, pgrandmother, \
            mstepsister, pstepsister = None, None, None, None, None, None, None, None, None

            if self.motherVar.get():
                mother = int(self.mother_age_spin.get())
            if self.daughterVar.get():
                daughter = [int(self.daughter_age_spin.get())]
            if self.sisterVar.get():
                sister = [int(self.sister_age_spin.get())]
            if self.mauntVar.get():
                maunt = [int(self.maunt_age_spin.get())]
            if self.pauntVar.get():
                paunt = [int(self.paunt_age_spin.get())]
            if self.mgrandmotherVar.get():
                mgrandmother = [int(self.mgrandmother_age_spin.get())]
            if self.pgrandmotherVar.get():
                pgrandmother = [int(self.pgrandmother_age_spin.get())]
            if self.mhalfsisterVar.get():
                mstepsister = [int(self.mstepsister_age_spin.get())]
            if self.phalfsisterVar.get():
                pstepsister = [int(self.pstepsister_age_spin.get())]

            claus_result = claus.calculate_risk(int(self.patient_age_spin.get()), mother, daughter, sister,
                                                maunt, paunt, mgrandmother, pgrandmother, mstepsister,
                                                pstepsister)

            if claus_result is None:
                claus_result = 0

            self.resultClaus.grid()
            self.resultClaus.configure(text='{:0.2f} %'.format((claus_result*100)))
        else:
            if int(self.age_spin.get()) > 85:
                age = 85
            elif int(self.age_spin.get()) < 35:
                age = 35
            else:
                age = int(self.age_spin.get())

            age_indicator = 0 if age < 50 else 1

            gailMod = gail.GailRiskCalculator()
            gailMod.Initialize()

            rhyp = np.float64(1.0)
            if HAD_BIOPSIES.get(self.biopsies.get()) == 1:
                if ATYPICAL_BIOPSIES.get(self.atypical_biopsies.get()) == 0:
                    rhyp = np.float64(0.93)
                elif ATYPICAL_BIOPSIES.get(self.atypical_biopsies.get()) == 1:
                    rhyp = np.float(1.82)

            fiveYearABS = gailMod.CalculateAbsoluteRisk(age,  # CurrentAge int    [t1]
                                                        age + 5,  # ProjectionAge int    [t2]
                                                        age_indicator,  # AgeIndicator int    [i0]
                                                        BIOPSIES.get(self.biopsies.get()),  # NumberOfBiopsy int    [i2]
                                                        MENARCH.get(self.menstruation.get()),  # MenarcheAge int    [i1]
                                                        BIRTH.get(self.birth.get()),  # FirstLiveBirthAge int    [i3]
                                                        HAD_BIOPSIES.get(self.biopsies.get()),  # EverHaveBiopsy int [iever]
                                                        RELATIVES.get(self.relatives.get()),  # FirstDegRelatives int    [i4]
                                                        ATYPICAL_BIOPSIES.get(self.atypical_biopsies.get()),  # int[ihyp]HyperPlasia
                                                        np.float64(1.82),  # double [rhyp]  RHyperPlasia
                                                        RACE.get(self.race.get())  # irace int    [race]
                                                        )
            fiveYearAVE = gailMod.CalculateAeverageRisk(age,
                                                        age + 5,
                                                        age_indicator,
                                                        BIOPSIES.get(self.biopsies.get()),
                                                        MENARCH.get(self.menstruation.get()),
                                                        BIRTH.get(self.birth.get()),
                                                        HAD_BIOPSIES.get(self.biopsies.get()),
                                                        RELATIVES.get(self.relatives.get()),
                                                        ATYPICAL_BIOPSIES.get(self.atypical_biopsies.get()),
                                                        rhyp,
                                                        RACE.get(self.race.get()))
            lifetimeABS = gailMod.CalculateAbsoluteRisk(age,
                                                        90,
                                                        age_indicator,
                                                        BIOPSIES.get(self.biopsies.get()),
                                                        MENARCH.get(self.menstruation.get()),
                                                        BIRTH.get(self.birth.get()),
                                                        HAD_BIOPSIES.get(self.biopsies.get()),
                                                        RELATIVES.get(self.relatives.get()),
                                                        ATYPICAL_BIOPSIES.get(self.atypical_biopsies.get()),
                                                        rhyp,
                                                        RACE.get(self.race.get()))
            lifetimeAve = gailMod.CalculateAeverageRisk(age,
                                                        90,
                                                        age_indicator,
                                                        BIOPSIES.get(self.biopsies.get()),
                                                        MENARCH.get(self.menstruation.get()),
                                                        BIRTH.get(self.birth.get()),
                                                        HAD_BIOPSIES.get(self.biopsies.get()),
                                                        RELATIVES.get(self.relatives.get()),
                                                        ATYPICAL_BIOPSIES.get(self.atypical_biopsies.get()),
                                                        rhyp,
                                                        RACE.get(self.race.get()))

            self.fYAresult.grid()
            self.fYAresult.configure(text='{:0.2f} %'.format(fiveYearABS*100))

            self.fYVresult.grid()
            self.fYVresult.configure(text='{:0.2f} %'.format(fiveYearAVE*100))

            self.lTAresult.grid()
            self.lTAresult.configure(text='{:0.2f} %'.format(lifetimeABS*100))

            self.lTVresult.grid()
            self.lTVresult.configure(text='{:0.2f} %'.format(lifetimeAve*100))


# ======================
# Start gui
# ======================
gui = GUI()
gui.main.mainloop()
